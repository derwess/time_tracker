package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/swaggo/files"
	"github.com/swaggo/gin-swagger"
	"time_tracker/internal/app/timetracker"
	"time_tracker/internal/pkg/config"
	"time_tracker/internal/pkg/http"
	"time_tracker/internal/pkg/log"

	_ "time_tracker/docs"
)

// @title Time Tracker API
// @version 1.0
// @description Tool for monitoring user tasks and time spent.

// @host localhost:8080
// @BasePath /api/

func main() {
	/*
		Using .env files can sometimes be a bit hard (due to syntax),
		so I prefer to use .yml files instead. They're much more concise.
	*/
	conf := config.NewConfig()

	logger := log.NewLogger(conf)
	gin.SetMode(gin.ReleaseMode)
	r := gin.Default()
	timetracker.SetupApp(r, conf, logger)
	logger.Info("Starting server")

	// Swagger API http://localhost:8080/swagger/index.html
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	// Plug for api call
	r.GET("/info", func(ctx *gin.Context) {
		passSer := ctx.Query("passportSerie")
		passNum := ctx.Query("passportNumber")
		if passSer == "" || passNum == "" {
			ctx.JSON(400, gin.H{"message": "bad request"})
			return
		}
		ctx.JSON(200, gin.H{
			"surname":    "Иванов",
			"name":       "Иван",
			"patronymic": "Иванович",
			"address":    "г. Москва, ул. Ленина, д. 5, кв. 1",
		})
	})

	http.Run(r, fmt.Sprintf("%s:%d", conf.GetString("http_address"), conf.GetInt("http_port")))
}
