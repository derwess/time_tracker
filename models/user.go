package models

import (
	"strings"
	"time_tracker/internal/pkg/helper/errs"
)

// User can also use UUID instead of ID. Will help with merging microservices.
type User struct {
	ID             int    `gorm:"primary_key;AUTO_INCREMENT"`
	Name           string `json:"name" gorm:"not null;default:null"`
	Surname        string `json:"surname" gorm:"not null;default:null"`
	Patronymic     string `json:"patronymic" gorm:"default:null"`
	PassportNumber string `json:"passportNumber" gorm:"not null;default:null"`
	Address        string `json:"address" gorm:"not null;default:null"`
}

type UserCreate struct {
	PassportNumber string `json:"passportNumber"`
}

func (u *User) FillDefault() {
	if u.Name == "" {
		u.Name = "Unknown"
	}
	if u.Surname == "" {
		u.Surname = "Unknown"
	}
	if u.Address == "" {
		u.Address = "Unknown"
	}
}

func (u *UserCreate) Validate() errs.Error {
	if u.PassportNumber == "" {
		return validationError()
	}
	if len(strings.Split(u.PassportNumber, " ")) != 2 {
		return validationError()
	}
	// Checks for password types (Number and Series) can also be implemented
	return nil
}
