package models

import (
	"time"
	"time_tracker/internal/pkg/helper/errs"
)

// Task can also use UUID instead of ID. Will help with merging microservices.
type Task struct {
	ID         int       `gorm:"primary_key;AUTO_INCREMENT"`
	Name       string    `json:"name" gorm:"not null"`
	AssigneeID int       `json:"assignee" gorm:"not null"`
	StartedAt  time.Time `json:"started_at" gorm:"not null;default:current_timestamp"`
	EndedAt    time.Time `json:"ended_at" gorm:"default:null"`
	TimeSpent  uint64    `json:"time_spent,omitempty" gorm:"default:null"`
}

type TaskCreate struct {
	Name string `json:"name"`
}

func (t *Task) FillDefault() {
	if t.Name == "" {
		t.Name = randomString(10)
	}
}

func (t *TaskCreate) Validate() errs.Error {
	if t.Name == "" {
		return validationError()
	}
	return nil
}
