package timetracker

import (
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"gorm.io/gorm"
	database "time_tracker/internal/pkg/db"
	"time_tracker/internal/pkg/log"
	"time_tracker/internal/pkg/middleware"
	"time_tracker/internal/pkg/routes"
	"time_tracker/internal/pkg/task"
	"time_tracker/internal/pkg/user"
)

func setupPostgres(conf *viper.Viper, logger *log.Logger) *gorm.DB {
	db := database.New(conf, logger)
	return db
}

func setupUserHandler(logger *log.Logger, db *gorm.DB) user.Handler {
	return user.NewHandler(logger, db)
}

func setupTaskHandler(logger *log.Logger, db *gorm.DB) task.Handler {
	return task.NewHandler(logger, db)
}

func setupRoutes(r *gin.Engine, userHandler user.Handler, taskHandler task.Handler) {
	r.Use(middleware.CORSMiddleware())
	api := r.Group("/api")
	{
		routes.UsersGroup(api, userHandler)

		// Current implementation does not support resuming of tasks
		routes.TasksGroup(api, taskHandler)
	}
}

func SetupApp(r *gin.Engine, cfg *viper.Viper, logger *log.Logger) {
	db := setupPostgres(cfg, logger)
	userHandler := setupUserHandler(logger, db)
	taskHandler := setupTaskHandler(logger, db)
	setupRoutes(r, userHandler, taskHandler)
}
