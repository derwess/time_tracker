package user

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
	"gorm.io/gorm"
	"io"
	"net/http"
	"strconv"
	"strings"
	"time_tracker/internal/pkg/helper/errs"
	"time_tracker/internal/pkg/helper/resp"
	"time_tracker/internal/pkg/log"
	"time_tracker/models"
)

type Handler interface {
	CreateUser(*gin.Context)
	UpdateUser(*gin.Context)
	DeleteUser(*gin.Context)
	GetUsers(*gin.Context)
}

type userHandler struct {
	logger *log.Logger
	db     *gorm.DB
}

func NewHandler(logger *log.Logger, db *gorm.DB) Handler {
	return &userHandler{
		logger: logger,
		db:     db,
	}
}

// CreateUser godoc
// @Summary     Create a new user
// @Description Create a new user with provided details
// @Tags        users
// @Accept      json
// @Produce     json
// @Param       passportNumber body string true "Serie and number of user's passport" example(1234 567890)
// @Success     200     {object} resp.response
// @Failure     400     {object} errs.CustomError
// @Failure     500     {object} errs.CustomError
// @Router      /users/create [post]
func (u *userHandler) CreateUser(ctx *gin.Context) {
	var user models.UserCreate
	if err := ctx.ShouldBindJSON(&user); err != nil {
		resp.HandleError(ctx, &errs.ErrorInJson)
		return
	}
	if err := user.Validate(); err != nil {
		resp.HandleError(ctx, err)
		return
	}

	var fullUser = models.User{PassportNumber: user.PassportNumber}

	x := strings.Split(fullUser.PassportNumber, " ")
	passNum, passSer := x[0], x[1]

	// Passport serie and number should be accurately validated
	apiLink := fmt.Sprintf("http://127.0.0.1:8080/info?passportSerie=%s&passportNumber=%s", passSer, passNum)
	response, err := http.Get(apiLink)

	if err != nil {
		u.logger.Debug("Error while parsing api: ",
			zap.Error(err),
			zap.String("apiLink", apiLink),
			zap.Int("statusCode", response.StatusCode))

		if response.StatusCode == http.StatusNotFound {
			resp.HandleError(ctx, &errs.ErrorNoObject)
		} else {
			resp.HandleError(ctx, &errs.ErrorServer)
		}
		return
	}

	if response.StatusCode != http.StatusOK {
		resp.HandleError(ctx, &errs.ErrorServer)
		return
	}

	responseData, err := io.ReadAll(response.Body)
	if err := json.Unmarshal(responseData, &fullUser); err != nil {
		u.logger.Debug("Error while parsing json: ", zap.Error(err))
		resp.HandleError(ctx, &errs.ErrorServer)
		return
	}

	// If we don't have enough fields but API Call returned 200 OK
	fullUser.FillDefault()
	if err := u.db.Create(&fullUser).Error; err != nil {
		u.logger.Debug("Error in database: ", zap.Error(err))
		resp.HandleError(ctx, &errs.ErrorInDB)
		return
	}
	resp.HandleSuccess(ctx, &fullUser)
}

// UpdateUser godoc
// @Summary     Update user by ID
// @Description Update user details by ID
// @Tags        users
// @Accept      json
// @Produce     json
// @Param       id path string true "User ID" example(42)
// @Param       passportNumber body string false "Serie and number of user's passport" example(1234 567890)
// @Param       Name           body string false "User's name" example(Vasiliy)
// @Param       Surname        body string false "User's surname" example(Ivanov)
// @Param       Patronymic     body string false "User's patronymic" example(Jovanovich)
// @Param       Address        body string false "User's address" example(Burkina str. 42)
// @Success     200     {object} models.User
// @Failure     404     {object} errs.CustomError
// @Failure     400     {object} errs.CustomError
// @Router      /users/update/{id} [post]
func (u *userHandler) UpdateUser(ctx *gin.Context) {
	// Also can be implemented with passport_number if unique
	id := ctx.Param("id")

	var user models.User
	if err := u.db.Where("id = ?", id).First(&user).Error; err != nil {
		if !errors.Is(err, gorm.ErrRecordNotFound) {
			u.logger.Error("Error in database: ", zap.Error(err))
		}
		resp.HandleError(ctx, &errs.ErrorNoObject)
		return
	}

	if err := ctx.ShouldBindJSON(&user); err != nil {
		resp.HandleError(ctx, &errs.ErrorInJson)
		return
	}

	// Won't update default fields
	u.db.Save(&user)

	ctx.JSON(http.StatusOK, user)
}

// DeleteUser godoc
// @Summary     Delete a user by ID
// @Description Delete a user by ID
// @Tags        users
// @Accept      json
// @Produce     json
// @Param       id path string true "User ID" example(42)
// @Success     200     {object} resp.response
// @Failure     404     {object} errs.CustomError
// @Router      /users/delete/{id} [post]
func (u *userHandler) DeleteUser(ctx *gin.Context) {
	// Also can be implemented with passport_number if unique
	id := ctx.Param("id")

	var user models.User
	if err := u.db.Where("id = ?", id).First(&user).Error; err != nil {
		if !errors.Is(err, gorm.ErrRecordNotFound) {
			u.logger.Error("Error in database: ", zap.Error(err))
		}
		resp.HandleError(ctx, &errs.ErrorNoObject)
		return
	}

	u.db.Delete(&user)
	resp.HandleSuccess(ctx, map[string]string{"message": "User deleted successfully"})
}

// GetUsers godoc
// @Summary     Get users with optional filtering and pagination
// @Description Retrieve users optionally filtered by name, surname, patronymic, passport number, or address, and paginated results
// @Tags        users
// @Accept      json
// @Produce     json
// @Param       page           query int false "Pagination Page" example(1)
// @Param       limit          query int false "Pagination Limit" example(5)
// @Param       name           query string false "Filter by name" example(Vasiliy)
// @Param       surname        query string false "Filter by surname" example(Ivanov)
// @Param       patronymic     query string false "Filter by patronymic" example(Jovanovich)
// @Param       passportNumber query string false "Filter by passport number" example(1234 567890)
// @Param       address        query string false "Filter by address" example(Burkina str. 42)
// @Success     200     {object} []models.User
// @Failure     400     {object} errs.CustomError
// @Router      /users [get]
func (u *userHandler) GetUsers(ctx *gin.Context) {
	page, err := strconv.Atoi(ctx.DefaultQuery("page", "1"))
	if err != nil || page <= 0 {
		resp.HandleError(ctx, &errs.ErrorInJson)
	}
	limit, err := strconv.Atoi(ctx.DefaultQuery("limit", "10"))
	if err != nil || limit <= 0 {
		resp.HandleError(ctx, &errs.ErrorInJson)
	}

	var users []models.User
	query := u.db.Model(&models.User{})
	filters := ctx.Request.URL.Query()
	for key, values := range filters {
		switch key {
		case "name":
			query = query.Where("name IN (?)", values)
		case "surname":
			query = query.Where("surname IN (?)", values)
		case "patronymic":
			query = query.Where("patronymic IN (?)", values)
		case "passportNumber":
			query = query.Where("passport_number IN (?)", values)
		case "address":
			query = query.Where("address IN (?)", values)
		}
	}
	paginator := resp.NewPaginator(0, page, limit)
	// Possible to use cursor if needed
	query.Offset(paginator.Offset).Limit(paginator.Limit).Find(&users)

	resp.HandleSuccess(ctx, users)
}
