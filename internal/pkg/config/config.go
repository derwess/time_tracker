package config

import (
	"os"

	"github.com/spf13/viper"
)

func NewConfig() *viper.Viper {
	configPath := os.Getenv("CONFIG_PATH")
	if configPath == "" {
		// If CONFIG_PATH is not set, defaults to the current directory.
		configPath = "."
	}

	configName := os.Getenv("CONFIG_NAME")
	if configName == "" {
		configName = "app"
	}

	configType := os.Getenv("CONFIG_TYPE")
	if configType == "" {
		configType = "env"
	}

	return getConfig(configPath, configName, configType)
}

// getConfig loads the configuration from the specified path, file name, and file type.
func getConfig(path, name, fileType string) *viper.Viper {
	conf := viper.New()
	conf.AddConfigPath(path)
	conf.SetConfigName(name)
	conf.SetConfigType(fileType)

	err := conf.ReadInConfig()
	if err != nil {
		// We rely on .env variables; therefore panic.
		panic(err)
	}

	return conf
}
