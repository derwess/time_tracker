package task

import (
	"errors"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
	"gorm.io/gorm"
	"strconv"
	"time"
	"time_tracker/internal/pkg/helper/errs"
	"time_tracker/internal/pkg/helper/resp"
	"time_tracker/internal/pkg/log"
	"time_tracker/models"
)

type Handler interface {
	GetTasks(*gin.Context)
	StartTask(*gin.Context)
	StopTaskLast(*gin.Context)
	StopTaskByID(*gin.Context)
}

type taskHandler struct {
	logger *log.Logger
	db     *gorm.DB
}

func NewHandler(logger *log.Logger, db *gorm.DB) Handler {
	return &taskHandler{
		logger: logger,
		db:     db,
	}
}

// GetTasks godoc
// @Summary     Get User's tasks
// @Tags        tasks
// @Description returns user's tasks sorted by spent time. Uses pagination
// @ID          get-tasks
// @Produce     json
// @Param       user_id         path    int    true  "User ID" example(42)
// @Param       page            query   int    false "Pagination Page" example(1)
// @Param       limit           query   int    false "Pagination Limit" example(5)
// @Param       from            query   string false "Start of the interval"  format(date-time) example(2023-07-03T10:34:56.789Z)
// @Param       to              query   string false "End of the interval"    format(date-time) example(2024-03-03T10:34:56.789Z)
// @Param       exclude_before  query   bool   false "Exclude Start < ReqStart, ReqStart <= End <= ReqEnd" example(false)
// @Param       exclude_after   query   bool   false "Exclude End > ReqEnd, ReqStart <= Start <= ReqEnd" example(false)
// @Success     200     {object} []models.Task
// @Failure     400     {object} errs.CustomError
// @Failure     404     {object} errs.CustomError
// @Failure     500     {object} errs.CustomError
// @Router      /tasks/{user_id} [get]
func (t *taskHandler) GetTasks(ctx *gin.Context) {
	userID := ctx.Param("user_id")
	if userID == "" {
		resp.HandleError(ctx, &errs.ErrorNoObject)
		return
	}

	// Pagination
	page, err := strconv.Atoi(ctx.DefaultQuery("page", "1"))
	if err != nil || page <= 0 {
		resp.HandleError(ctx, &errs.ErrorInJson)
		return
	}
	limit, err := strconv.Atoi(ctx.DefaultQuery("limit", "10"))
	if err != nil || limit <= 0 {
		resp.HandleError(ctx, &errs.ErrorInJson)
		return
	}

	var excludeBefore, excludeAfter bool
	var timeFrom, timeTo time.Time

	// Making sure that end point is current time and start point is not zero
	//timeFrom = time.Date(1970, time.December, 1, 0, 0, 0, 0, time.UTC)
	timeTo = time.Now()

	from := ctx.Query("from")
	to := ctx.Query("to")

	excludeBefore, err = strconv.ParseBool(ctx.Query("exclude_before"))
	if err != nil {
		excludeBefore = false
	}

	excludeAfter, err = strconv.ParseBool(ctx.Query("exclude_after"))
	if err != nil {
		excludeAfter = false
	}

	if from != "" {
		timeFrom, err = time.Parse(time.RFC3339, from)
		if err != nil {
			t.logger.Error("Error while parsing start interval time: ",
				zap.Error(err),
				zap.String("timestamp", from),
			)
			resp.HandleError(ctx, &errs.ErrorInJson)
			return
		}
	}

	if to != "" {
		timeTo, err = time.Parse(time.RFC3339, to)
		if err != nil {
			t.logger.Error("Error while parsing end interval time: ",
				zap.Error(err),
				zap.String("timestamp", to),
			)
			resp.HandleError(ctx, &errs.ErrorInJson)
			return
		}
	}

	if timeFrom.Sub(timeTo).Seconds() > 0 {
		t.logger.Debug("End time is after start time: ",
			zap.String("start", from),
			zap.String("end", to),
			zap.Float64("difference", timeFrom.Sub(timeTo).Seconds()),
		)
		resp.HandleError(ctx, &errs.ErrorWrongParams)
		return
	}
	var tasks []models.Task

	paginator := resp.NewPaginator(0, page, limit)

	query := t.db.Where("assignee_id = ?", userID)

	// If at least one of the ends belongs to the interval
	query = query.Where("(started_at BETWEEN ? AND ?) OR (ended_at BETWEEN ? AND ?)", timeFrom, timeTo, timeFrom, timeTo)

	if excludeBefore {
		query = query.Where("started_at >= ?", timeFrom)
	}
	if excludeAfter {
		query = query.Where("ended_at <= ?", timeTo)
	}

	query = query.Order("time_spent desc, id desc")
	query = query.Offset(paginator.Offset).Limit(paginator.Limit)
	query.Find(&tasks)

	resp.HandleSuccess(ctx, tasks, paginator)
}

// StartTask godoc
// @Summary     Start a new task
// @Tags        tasks
// @Description Start a new task for a user
// @Accept      json
// @Produce     json
// @Param       user_id path  string     true  "User ID" example(42)
// @Param       task    body  string     true  "Creating task's name" example(Create new project)
// @Success     200     {object} models.Task
// {ID: 42, Name: "New Task", AssigneeID: 42, StartedAt: "2023-07-03T10:34:56.789Z"}
// @Failure     400     {object} errs.CustomError
// @Failure     400     {object} errs.CustomError
// @Failure     404     {object} errs.CustomError
// @Failure     500     {object} errs.CustomError
// @Router      /tasks/{user_id}/start [post]
func (t *taskHandler) StartTask(ctx *gin.Context) {
	userID := ctx.Param("user_id")

	var user models.User
	if err := t.db.Where("id = ?", userID).First(&user).Error; err != nil {
		if !errors.Is(err, gorm.ErrRecordNotFound) {
			t.logger.Error("Error in database: ",
				zap.Error(err),
				zap.String("userID", userID),
			)
		}
		resp.HandleError(ctx, &errs.ErrorNoObject)
		return
	}

	var task models.TaskCreate
	if err := ctx.ShouldBindJSON(&task); err != nil {
		resp.HandleError(ctx, &errs.ErrorInJson)
		return
	}

	if err := task.Validate(); err != nil {
		resp.HandleError(ctx, err)
		return
	}

	var fullTask = models.Task{Name: task.Name}

	var err error
	fullTask.AssigneeID, err = strconv.Atoi(ctx.Param("user_id"))

	if err != nil {
		resp.HandleError(ctx, &errs.ErrorInJson)
		return
	}

	// Won't update GORM's default fields
	t.db.Save(&fullTask)

	if fullTask.ID == 0 {
		resp.HandleError(ctx, &errs.ErrorInDB)
		return
	}

	resp.HandleSuccess(ctx, &fullTask)
}

// StopTaskLast godoc
// @Summary     Stop the last started task for a user
// @Description Stop the last started task for a user, updating the EndedAt time and calculating the time spent
// @Tags        tasks
// @Accept      json
// @Produce     json
// @Param       user_id path  string     true  "User ID" example(42)
// @Success     200     {object} models.Task
// @Failure     403     {object} errs.CustomError
// @Failure     404     {object} errs.CustomError
// @Router      /tasks/{user_id}/stop [post]
func (t *taskHandler) StopTaskLast(ctx *gin.Context) {
	userID := ctx.Param("user_id")
	if userID == "" {
		resp.HandleError(ctx, &errs.ErrorNoObject)
	}

	var task models.Task
	if err := t.db.Order("id desc").Where("assignee_id = ?", userID).First(&task).Error; err != nil {
		if !errors.Is(err, gorm.ErrRecordNotFound) {
			t.logger.Error("Error in database: ",
				zap.Error(err),
				zap.String("assignee_id", userID),
			)
		}
		resp.HandleError(ctx, &errs.ErrorNoObject)
		return
	}

	// Make sure that task is not stopped
	if !task.EndedAt.IsZero() {
		resp.HandleError(ctx, &errs.ErrorForbidden)
		return
	}

	task.EndedAt = time.Now()

	// Max Time is ~30mil years
	task.TimeSpent = uint64(task.EndedAt.Sub(task.StartedAt).Seconds())

	// Won't update GORM's default fields
	t.db.Save(&task)

	resp.HandleSuccess(ctx, &task)
}

// StopTaskByID godoc
// @Summary     Stop a specific task by ID for a user
// @Description Stop a specific task by ID for a user, updating the EndedAt time and calculating the time spent
// @Tags        tasks
// @Accept      json
// @Produce     json
// @Param       user_id path  string     true  "User ID" example(42)
// @Param       task_id path  string     true  "Task ID" example(42)
// @Success     200     {object} models.Task
// @Failure     403     {object} errs.CustomError
// @Failure     404     {object} errs.CustomError
// @Router      /tasks/{user_id}/stop/{task_id} [post]
func (t *taskHandler) StopTaskByID(ctx *gin.Context) {
	taskID := ctx.Param("task_id")
	userID := ctx.Param("user_id")
	if taskID == "" || userID == "" {
		resp.HandleError(ctx, &errs.ErrorNoObject)
	}

	var task models.Task
	if err := t.db.Where("id = ?", taskID).First(&task).Error; err != nil {
		if !errors.Is(err, gorm.ErrRecordNotFound) {
			t.logger.Error("Error in database: ",
				zap.Error(err),
				zap.String("task_id", taskID),
			)
		}
		resp.HandleError(ctx, &errs.ErrorNoObject)
		return
	}

	userIntID, _ := strconv.Atoi(userID)
	// Make sure that task is not stopped and it belongs to the assignee
	if !task.EndedAt.IsZero() || (task.AssigneeID != userIntID) {
		resp.HandleError(ctx, &errs.ErrorForbidden)
		return
	}

	task.EndedAt = time.Now()

	// Max Time is ~30mil years
	task.TimeSpent = uint64(task.EndedAt.Sub(task.StartedAt).Seconds())

	// Won't update GORM's default fields
	t.db.Save(&task)

	resp.HandleSuccess(ctx, &task)
}
