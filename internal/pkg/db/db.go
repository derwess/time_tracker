package db

import (
	"fmt"
	"github.com/spf13/viper"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"time_tracker/internal/pkg/log"
	"time_tracker/models"

	_ "github.com/lib/pq"
)

type pqData struct {
	Host     string
	Port     int
	User     string
	Password string
	Name     string
}

func importData(conf *viper.Viper) *pqData {
	return &pqData{
		Host:     conf.GetString("pq_host"),
		Port:     conf.GetInt("pq_port"),
		User:     conf.GetString("pq_user"),
		Password: conf.GetString("pq_pass"),
		Name:     conf.GetString("pq_name"),
	}
}

func New(conf *viper.Viper, logger *log.Logger) *gorm.DB {
	connection := importData(conf)
	connectionString := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		connection.Host, connection.Port, connection.User, connection.Password, connection.Name)
	gormDB, err := gorm.Open(postgres.New(postgres.Config{
		DSN: connectionString,
	}), &gorm.Config{})
	if err != nil {
		logger.Fatal(fmt.Sprint("Couldn't connect Postgres", err))
	}

	logger.Info("Migrating Postgres")
	err = gormDB.AutoMigrate(&models.User{}, &models.Task{})
	if err != nil {
		logger.Fatal(fmt.Sprint("Couldn't migrate Postgres", err))
	}
	logger.Info("Postgres successfully migrated")
	logger.Info("gorm database connected")
	return gormDB
}
