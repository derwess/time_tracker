package resp

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"time_tracker/internal/pkg/helper/errs"
)

type response struct {
	Code      int         `json:"code"`
	Message   string      `json:"message"`
	Data      interface{} `json:"data"`
	Paginator *Paginator  `json:"paginator,omitempty"`
}

type Paginator struct {
	Total    int `json:"total"`
	PagesNum int `json:"pages_num"`
	Page     int `json:"page"`
	Offset   int `json:"offset"`
	Limit    int `json:"limit"`
}

func HandleSuccess(ctx *gin.Context, data interface{}, paginator ...*Paginator) {
	var resp response
	if data == nil {
		data = map[string]string{}
	}
	if len(paginator) > 0 {
		resp = response{Code: 200, Message: "success", Data: data, Paginator: paginator[0]}
	} else {
		resp = response{Code: 200, Message: "success", Data: data}
	}
	ctx.JSON(http.StatusOK, resp)
}

func HandleError(ctx *gin.Context, err errs.Error) {
	if err.Data() == nil {
		err.SetData(map[string]string{})
	}
	resp := response{Code: err.ErrorCode(), Message: err.Error(), Data: err.Data()}
	ctx.JSON(err.ErrorCode(), resp)
}

func NewPaginator(total int, page int, limit int) *Paginator {
	var pagesNum int
	if total%limit == 0 {
		pagesNum = total / limit
	} else {
		pagesNum = total/limit + 1
	}
	return &Paginator{
		Total:    total,
		PagesNum: pagesNum,
		Page:     page,
		Offset:   (page - 1) * limit,
		Limit:    limit,
	}
}
