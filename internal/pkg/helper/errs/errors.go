package errs

type Error interface {
	Error() string
	ErrorCode() int
	Data() interface{}
	SetData(interface{})
}

type CustomError struct {
	Message string
}

type err struct {
	error     CustomError
	errorCode int
	data      interface{}
}

func (e *err) Error() string {
	return e.error.Message
}

func (e *err) ErrorCode() int {
	return e.errorCode
}

func (e *err) Data() interface{} {
	return e.data
}

func (e *err) SetData(data interface{}) {
	e.data = data
}
