package errs

var ErrorInJson = err{error: CustomError{Message: `error in unmarshalling JSON`},
	errorCode: 400,
	data:      nil,
}
var ErrorInDB = err{error: CustomError{Message: `error in database`},
	errorCode: 400,
	data:      nil,
}
var ErrorWrongParams = err{error: CustomError{Message: `wrong params`},
	errorCode: 500,
	data:      nil,
}
var ErrorServer = err{error: CustomError{Message: `server error`},
	errorCode: 500,
	data:      nil,
}
var ErrorNoObject = err{error: CustomError{Message: `no object`},
	errorCode: 404,
	data:      nil,
}

var ErrorForbidden = err{error: CustomError{Message: `forbidden`},
	errorCode: 403,
	data:      nil,
}
