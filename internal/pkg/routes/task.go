package routes

import (
	"github.com/gin-gonic/gin"
	"time_tracker/internal/pkg/task"
)

func TasksGroup(r *gin.RouterGroup, handler task.Handler) {
	TaskGroup := r.Group("/tasks")
	{
		TaskGroup.GET("/:user_id", handler.GetTasks)
		TaskGroup.POST("/:user_id/start", handler.StartTask)

		// Stops last task created for user. Note that other tasks will still be active.
		TaskGroup.POST("/:user_id/stop", handler.StopTaskLast)

		// If user have several tasks atm, we need to have access to all of them
		TaskGroup.POST("/:user_id/stop/:task_id", handler.StopTaskByID)
	}
}
