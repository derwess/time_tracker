package routes

import (
	"github.com/gin-gonic/gin"
	"time_tracker/internal/pkg/user"
)

func UsersGroup(r *gin.RouterGroup, handler user.Handler) {
	userGroup := r.Group("/users")
	{
		userGroup.POST("/create", handler.CreateUser)
		userGroup.POST("/update/:id", handler.UpdateUser)
		userGroup.POST("/delete/:id", handler.DeleteUser)
		userGroup.GET("/", handler.GetUsers)
	}
}
