# How to Install

1. Install [Docker](https://docs.docker.com/engine/install/ubuntu/) and [Golang](https://go.dev/doc/install)

2. Run `git clone https://gitlab.com/derwess/time_tracker.git` to copy project on your pc.
3. Move to project's directory `cd ./time_tracker`.
4. Run `go mod download` to install all dependencies and `go run ./cmd/timetracker/main.go` to launch it locally (Make sure you have postgresql server running on your computer).
5. Or use `docker compose build && docker compose up` to build and run dockerized server.