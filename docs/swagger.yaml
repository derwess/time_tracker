basePath: /api/
definitions:
  errs.CustomError:
    properties:
      message:
        type: string
    type: object
  models.Task:
    properties:
      assignee:
        type: integer
      ended_at:
        type: string
      id:
        type: integer
      name:
        type: string
      started_at:
        type: string
      time_spent:
        type: integer
    type: object
  models.User:
    properties:
      address:
        type: string
      id:
        type: integer
      name:
        type: string
      passportNumber:
        type: string
      patronymic:
        type: string
      surname:
        type: string
    type: object
  resp.Paginator:
    properties:
      limit:
        type: integer
      offset:
        type: integer
      page:
        type: integer
      pages_num:
        type: integer
      total:
        type: integer
    type: object
  resp.response:
    properties:
      code:
        type: integer
      data: {}
      message:
        type: string
      paginator:
        $ref: '#/definitions/resp.Paginator'
    type: object
host: localhost:8080
info:
  contact: {}
  description: Tool for monitoring user tasks and time spent.
  title: Time Tracker API
  version: "1.0"
paths:
  /tasks/{user_id}:
    get:
      description: returns user's tasks sorted by spent time. Uses pagination
      operationId: get-tasks
      parameters:
      - description: User ID
        example: 42
        in: path
        name: user_id
        required: true
        type: integer
      - description: Pagination Page
        example: 1
        in: query
        name: page
        type: integer
      - description: Pagination Limit
        example: 5
        in: query
        name: limit
        type: integer
      - description: Start of the interval
        example: "2023-07-03T10:34:56.789Z"
        format: date-time
        in: query
        name: from
        type: string
      - description: End of the interval
        example: "2024-03-03T10:34:56.789Z"
        format: date-time
        in: query
        name: to
        type: string
      - description: Exclude Start < ReqStart, ReqStart <= End <= ReqEnd
        example: false
        in: query
        name: exclude_before
        type: boolean
      - description: Exclude End > ReqEnd, ReqStart <= Start <= ReqEnd
        example: false
        in: query
        name: exclude_after
        type: boolean
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            items:
              $ref: '#/definitions/models.Task'
            type: array
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/errs.CustomError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/errs.CustomError'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/errs.CustomError'
      summary: Get User's tasks
      tags:
      - tasks
  /tasks/{user_id}/start:
    post:
      consumes:
      - application/json
      description: Start a new task for a user
      parameters:
      - description: User ID
        example: "42"
        in: path
        name: user_id
        required: true
        type: string
      - description: Creating task's name
        example: Create new project
        in: body
        name: task
        required: true
        schema:
          type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/models.Task'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/errs.CustomError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/errs.CustomError'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/errs.CustomError'
      summary: Start a new task
      tags:
      - tasks
  /tasks/{user_id}/stop:
    post:
      consumes:
      - application/json
      description: Stop the last started task for a user, updating the EndedAt time
        and calculating the time spent
      parameters:
      - description: User ID
        example: "42"
        in: path
        name: user_id
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/models.Task'
        "403":
          description: Forbidden
          schema:
            $ref: '#/definitions/errs.CustomError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/errs.CustomError'
      summary: Stop the last started task for a user
      tags:
      - tasks
  /tasks/{user_id}/stop/{task_id}:
    post:
      consumes:
      - application/json
      description: Stop a specific task by ID for a user, updating the EndedAt time
        and calculating the time spent
      parameters:
      - description: User ID
        example: "42"
        in: path
        name: user_id
        required: true
        type: string
      - description: Task ID
        example: "42"
        in: path
        name: task_id
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/models.Task'
        "403":
          description: Forbidden
          schema:
            $ref: '#/definitions/errs.CustomError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/errs.CustomError'
      summary: Stop a specific task by ID for a user
      tags:
      - tasks
  /users:
    get:
      consumes:
      - application/json
      description: Retrieve users optionally filtered by name, surname, patronymic,
        passport number, or address, and paginated results
      parameters:
      - description: Pagination Page
        example: 1
        in: query
        name: page
        type: integer
      - description: Pagination Limit
        example: 5
        in: query
        name: limit
        type: integer
      - description: Filter by name
        example: Vasiliy
        in: query
        name: name
        type: string
      - description: Filter by surname
        example: Ivanov
        in: query
        name: surname
        type: string
      - description: Filter by patronymic
        example: Jovanovich
        in: query
        name: patronymic
        type: string
      - description: Filter by passport number
        example: 1234 567890
        in: query
        name: passportNumber
        type: string
      - description: Filter by address
        example: Burkina str. 42
        in: query
        name: address
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            items:
              $ref: '#/definitions/models.User'
            type: array
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/errs.CustomError'
      summary: Get users with optional filtering and pagination
      tags:
      - users
  /users/create:
    post:
      consumes:
      - application/json
      description: Create a new user with provided details
      parameters:
      - description: Serie and number of user's passport
        example: 1234 567890
        in: body
        name: passportNumber
        required: true
        schema:
          type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/resp.response'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/errs.CustomError'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/errs.CustomError'
      summary: Create a new user
      tags:
      - users
  /users/delete/{id}:
    post:
      consumes:
      - application/json
      description: Delete a user by ID
      parameters:
      - description: User ID
        example: "42"
        in: path
        name: id
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/resp.response'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/errs.CustomError'
      summary: Delete a user by ID
      tags:
      - users
  /users/update/{id}:
    post:
      consumes:
      - application/json
      description: Update user details by ID
      parameters:
      - description: User ID
        example: "42"
        in: path
        name: id
        required: true
        type: string
      - description: Serie and number of user's passport
        example: 1234 567890
        in: body
        name: passportNumber
        schema:
          type: string
      - description: User's name
        example: Vasiliy
        in: body
        name: Name
        schema:
          type: string
      - description: User's surname
        example: Ivanov
        in: body
        name: Surname
        schema:
          type: string
      - description: User's patronymic
        example: Jovanovich
        in: body
        name: Patronymic
        schema:
          type: string
      - description: User's address
        example: Burkina str. 42
        in: body
        name: Address
        schema:
          type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/models.User'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/errs.CustomError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/errs.CustomError'
      summary: Update user by ID
      tags:
      - users
swagger: "2.0"
