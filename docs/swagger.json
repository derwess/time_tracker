{
    "swagger": "2.0",
    "info": {
        "description": "Tool for monitoring user tasks and time spent.",
        "title": "Time Tracker API",
        "contact": {},
        "version": "1.0"
    },
    "host": "localhost:8080",
    "basePath": "/api/",
    "paths": {
        "/tasks/{user_id}": {
            "get": {
                "description": "returns user's tasks sorted by spent time. Uses pagination",
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "tasks"
                ],
                "summary": "Get User's tasks",
                "operationId": "get-tasks",
                "parameters": [
                    {
                        "type": "integer",
                        "example": 42,
                        "description": "User ID",
                        "name": "user_id",
                        "in": "path",
                        "required": true
                    },
                    {
                        "type": "integer",
                        "example": 1,
                        "description": "Pagination Page",
                        "name": "page",
                        "in": "query"
                    },
                    {
                        "type": "integer",
                        "example": 5,
                        "description": "Pagination Limit",
                        "name": "limit",
                        "in": "query"
                    },
                    {
                        "type": "string",
                        "format": "date-time",
                        "example": "2023-07-03T10:34:56.789Z",
                        "description": "Start of the interval",
                        "name": "from",
                        "in": "query"
                    },
                    {
                        "type": "string",
                        "format": "date-time",
                        "example": "2024-03-03T10:34:56.789Z",
                        "description": "End of the interval",
                        "name": "to",
                        "in": "query"
                    },
                    {
                        "type": "boolean",
                        "example": false,
                        "description": "Exclude Start \u003c ReqStart, ReqStart \u003c= End \u003c= ReqEnd",
                        "name": "exclude_before",
                        "in": "query"
                    },
                    {
                        "type": "boolean",
                        "example": false,
                        "description": "Exclude End \u003e ReqEnd, ReqStart \u003c= Start \u003c= ReqEnd",
                        "name": "exclude_after",
                        "in": "query"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/models.Task"
                            }
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/errs.CustomError"
                        }
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {
                            "$ref": "#/definitions/errs.CustomError"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/errs.CustomError"
                        }
                    }
                }
            }
        },
        "/tasks/{user_id}/start": {
            "post": {
                "description": "Start a new task for a user",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "tasks"
                ],
                "summary": "Start a new task",
                "parameters": [
                    {
                        "type": "string",
                        "example": "42",
                        "description": "User ID",
                        "name": "user_id",
                        "in": "path",
                        "required": true
                    },
                    {
                        "example": "Create new project",
                        "description": "Creating task's name",
                        "name": "task",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "type": "string"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/models.Task"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/errs.CustomError"
                        }
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {
                            "$ref": "#/definitions/errs.CustomError"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/errs.CustomError"
                        }
                    }
                }
            }
        },
        "/tasks/{user_id}/stop": {
            "post": {
                "description": "Stop the last started task for a user, updating the EndedAt time and calculating the time spent",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "tasks"
                ],
                "summary": "Stop the last started task for a user",
                "parameters": [
                    {
                        "type": "string",
                        "example": "42",
                        "description": "User ID",
                        "name": "user_id",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/models.Task"
                        }
                    },
                    "403": {
                        "description": "Forbidden",
                        "schema": {
                            "$ref": "#/definitions/errs.CustomError"
                        }
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {
                            "$ref": "#/definitions/errs.CustomError"
                        }
                    }
                }
            }
        },
        "/tasks/{user_id}/stop/{task_id}": {
            "post": {
                "description": "Stop a specific task by ID for a user, updating the EndedAt time and calculating the time spent",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "tasks"
                ],
                "summary": "Stop a specific task by ID for a user",
                "parameters": [
                    {
                        "type": "string",
                        "example": "42",
                        "description": "User ID",
                        "name": "user_id",
                        "in": "path",
                        "required": true
                    },
                    {
                        "type": "string",
                        "example": "42",
                        "description": "Task ID",
                        "name": "task_id",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/models.Task"
                        }
                    },
                    "403": {
                        "description": "Forbidden",
                        "schema": {
                            "$ref": "#/definitions/errs.CustomError"
                        }
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {
                            "$ref": "#/definitions/errs.CustomError"
                        }
                    }
                }
            }
        },
        "/users": {
            "get": {
                "description": "Retrieve users optionally filtered by name, surname, patronymic, passport number, or address, and paginated results",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "users"
                ],
                "summary": "Get users with optional filtering and pagination",
                "parameters": [
                    {
                        "type": "integer",
                        "example": 1,
                        "description": "Pagination Page",
                        "name": "page",
                        "in": "query"
                    },
                    {
                        "type": "integer",
                        "example": 5,
                        "description": "Pagination Limit",
                        "name": "limit",
                        "in": "query"
                    },
                    {
                        "type": "string",
                        "example": "Vasiliy",
                        "description": "Filter by name",
                        "name": "name",
                        "in": "query"
                    },
                    {
                        "type": "string",
                        "example": "Ivanov",
                        "description": "Filter by surname",
                        "name": "surname",
                        "in": "query"
                    },
                    {
                        "type": "string",
                        "example": "Jovanovich",
                        "description": "Filter by patronymic",
                        "name": "patronymic",
                        "in": "query"
                    },
                    {
                        "type": "string",
                        "example": "1234 567890",
                        "description": "Filter by passport number",
                        "name": "passportNumber",
                        "in": "query"
                    },
                    {
                        "type": "string",
                        "example": "Burkina str. 42",
                        "description": "Filter by address",
                        "name": "address",
                        "in": "query"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/models.User"
                            }
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/errs.CustomError"
                        }
                    }
                }
            }
        },
        "/users/create": {
            "post": {
                "description": "Create a new user with provided details",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "users"
                ],
                "summary": "Create a new user",
                "parameters": [
                    {
                        "example": "1234 567890",
                        "description": "Serie and number of user's passport",
                        "name": "passportNumber",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "type": "string"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/resp.response"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/errs.CustomError"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/errs.CustomError"
                        }
                    }
                }
            }
        },
        "/users/delete/{id}": {
            "post": {
                "description": "Delete a user by ID",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "users"
                ],
                "summary": "Delete a user by ID",
                "parameters": [
                    {
                        "type": "string",
                        "example": "42",
                        "description": "User ID",
                        "name": "id",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/resp.response"
                        }
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {
                            "$ref": "#/definitions/errs.CustomError"
                        }
                    }
                }
            }
        },
        "/users/update/{id}": {
            "post": {
                "description": "Update user details by ID",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "users"
                ],
                "summary": "Update user by ID",
                "parameters": [
                    {
                        "type": "string",
                        "example": "42",
                        "description": "User ID",
                        "name": "id",
                        "in": "path",
                        "required": true
                    },
                    {
                        "example": "1234 567890",
                        "description": "Serie and number of user's passport",
                        "name": "passportNumber",
                        "in": "body",
                        "schema": {
                            "type": "string"
                        }
                    },
                    {
                        "example": "Vasiliy",
                        "description": "User's name",
                        "name": "Name",
                        "in": "body",
                        "schema": {
                            "type": "string"
                        }
                    },
                    {
                        "example": "Ivanov",
                        "description": "User's surname",
                        "name": "Surname",
                        "in": "body",
                        "schema": {
                            "type": "string"
                        }
                    },
                    {
                        "example": "Jovanovich",
                        "description": "User's patronymic",
                        "name": "Patronymic",
                        "in": "body",
                        "schema": {
                            "type": "string"
                        }
                    },
                    {
                        "example": "Burkina str. 42",
                        "description": "User's address",
                        "name": "Address",
                        "in": "body",
                        "schema": {
                            "type": "string"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/models.User"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/errs.CustomError"
                        }
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {
                            "$ref": "#/definitions/errs.CustomError"
                        }
                    }
                }
            }
        }
    },
    "definitions": {
        "errs.CustomError": {
            "type": "object",
            "properties": {
                "message": {
                    "type": "string"
                }
            }
        },
        "models.Task": {
            "type": "object",
            "properties": {
                "assignee": {
                    "type": "integer"
                },
                "ended_at": {
                    "type": "string"
                },
                "id": {
                    "type": "integer"
                },
                "name": {
                    "type": "string"
                },
                "started_at": {
                    "type": "string"
                },
                "time_spent": {
                    "type": "integer"
                }
            }
        },
        "models.User": {
            "type": "object",
            "properties": {
                "address": {
                    "type": "string"
                },
                "id": {
                    "type": "integer"
                },
                "name": {
                    "type": "string"
                },
                "passportNumber": {
                    "type": "string"
                },
                "patronymic": {
                    "type": "string"
                },
                "surname": {
                    "type": "string"
                }
            }
        },
        "resp.Paginator": {
            "type": "object",
            "properties": {
                "limit": {
                    "type": "integer"
                },
                "offset": {
                    "type": "integer"
                },
                "page": {
                    "type": "integer"
                },
                "pages_num": {
                    "type": "integer"
                },
                "total": {
                    "type": "integer"
                }
            }
        },
        "resp.response": {
            "type": "object",
            "properties": {
                "code": {
                    "type": "integer"
                },
                "data": {},
                "message": {
                    "type": "string"
                },
                "paginator": {
                    "$ref": "#/definitions/resp.Paginator"
                }
            }
        }
    }
}